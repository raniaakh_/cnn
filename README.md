# cnn

This code is able to classify images into multiple class and produce the DET Curve by merge some class into 2 class order to fit on DET Curve.

1. Make sure you already put image set into one folder and sub-folder as category
2. Install the depedencies: tensorflow, keras, numpy, matpolib, sklearn, cv2 and tqdm.
3. Running code on your compiler or your terminal by typing python3 image_classification.py