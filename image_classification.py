# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 12:50:09 2019

@author: raniaakh
"""

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.constraints import unit_norm
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Activation, Flatten

import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from tqdm import tqdm
from sklearn.model_selection import train_test_split

gpu_options = tf.compat.v1.GPUOptions(allow_growth=True)

TRAIN_DIR = r"D:\Laboratory\AOI\CameraNumber13\train"
CATEGORIES = ["D", "C", "B", "A"]
global fpr_all, fnr_all
fpr_all = []
fnr_all = []

def create_training_data():
    global all_x_data, all_y_data
    all_x_data = []
    all_y_data = []
    training_data = []
    for category in CATEGORIES:  # do categories
        path = os.path.join(TRAIN_DIR,category)
        for img in os.listdir(path):  # iterate over each image 
            img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
    
            break  # we just want one for now so break
        break  #...and one more!

    IMG_SIZE = 128
    
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    
    for category in CATEGORIES:  # do categories

        path = os.path.join(TRAIN_DIR,category)  # create path 
        class_num = CATEGORIES.index(category)  # get the classification  (0 or a 1). 0=dog 1=cat

        for img in tqdm(os.listdir(path)):  # iterate over each image per 
            try:
                img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # convert to array
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))  # resize to normalize data size
                training_data.append([new_array, class_num])  # add this to our training_data
            except Exception:  # in the interest in keeping the output clean...
                pass
        
    for features,label in training_data:
        all_x_data.append(features)    
        all_y_data.append(label)
    
    all_x_data = np.array(all_x_data).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
        
def training():
    model = Sequential()
    model.add(Conv2D(8, (3, 3), padding="same", input_shape=X_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(3, 3)))
    model.add(Conv2D(16, (3, 3), padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(3, 3)))
    model.add(Conv2D(32, (3, 3), padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64, (2, 2), padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
      
    model.add(Flatten())
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu', kernel_constraint=unit_norm()))
    model.add(Dense(256, activation='relu', kernel_constraint=unit_norm()))
    model.add(Dense(4, activation='softmax', kernel_constraint=unit_norm()))
    
    model.compile(loss='sparse_categorical_crossentropy', 
                  optimizer=keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                  metrics=['accuracy'])
    
    #print("Loss: ", score[0], "Accuracy: ", score[1])
    #es = EarlyStopping(monitor='val_acc', mode='max', verbose=1, patience=10)
    #model.fit(X_train, y_train, epochs=50, validation_split=0.1, verbose=1, batch_size = 16, callbacks=[es], shuffle=True)
    model.fit(X_train, y_train, epochs=25, validation_split=0.1, verbose=1, batch_size = 16, shuffle=True)
    scores = model.evaluate(X_train, y_train, verbose=0)
    model.save_weights('cnn_model_4c_t1112.h5')  # always save your weights after training or during training
    model.save('cnn_model_4c_t1112.tflearn')
    del model
    print("Training finished")
    return round(scores[1],2), round(scores[0],2)

def testing():
    from tensorflow.keras.models import load_model
    
    model = load_model('cnn_model_4c_t1112.tflearn')
    score = model.evaluate(X_test, y_test, verbose=0)
    # add code for calculate: probability estimation
    # add code for modifying class label and probability estimation
    # add code for ROC curve
    
    # get probability estimation
    y_prob = model.predict_proba(X_test)
    
    #get classes
    y_pred = model.predict_classes(X_test)
    
    # delete class A, then sum class B, class C, class D to get the total non_defect probability
    non_defect_prob = np.sum(np.delete(y_prob, 0, 1), axis=1)
    
    # defect probability = class A
    defect_prob = y_prob[:,0]
    
    # merge non defect and defect probability to calculate roc_curve
    np.vstack((non_defect_prob, defect_prob)).T
    
    # change class label. Class 0 -> 0 (defect), class 1,2,3 -> 1 (non-defect)
    new_y_test = np.where(np.array(y_test) > 0, 1, 0)
    draw_DET_curve(new_y_test, non_defect_prob)
    print("Testing finished")
    from sklearn.metrics import classification_report, confusion_matrix
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    del model
    return round(score[1],2), round(score[0],2) 
    
def draw_DET_curve (y_label, y_prob):  
    
    # count the number of defect and non defect
    total_non_defect = np.count_nonzero(y_label)
    total_defect = len(y_label) - total_non_defect
    
    # set threshold interval
    x = np.linspace (0, 1, num=100)
    
    # declare FPR and FNR array
    FPR_list = []
    FNR_list = []
    
    # iterate through all values of x
    for i in range(len(x)):
        # cumulative sum, reset if we finish calculate FNR and FPR for each threshold
        cum_FN = 0
        cum_FP = 0
    
        for j in range(len(y_prob)):
            # for defect data (y_label == 0), we calculate in number of false negative
            if y_label[j] < 1:
                if y_prob[j] > x[i]:
                   cum_FN = cum_FN + 1
            else:  
            #for non-defect data (y_label == 1), we calculate the number of false positive
                if y_prob[j] < x[i]:
                   cum_FP = cum_FP + 1
                   
        FNR=cum_FN/total_defect
        FPR=cum_FP/total_non_defect
        
        FPR_list.append(FPR)
        FNR_list.append(FNR)
  
    fpr_all.append(FPR_list)
    fnr_all.append(FNR_list)
    
if __name__ == "__main__":
    acc_tr = []
    acc_ts = []
    lr_tr = []
    lr_ts = []
    
    with tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True, log_device_placement=True)):
        for i in range(50):
            print(i)
            create_training_data()
            X_train, X_test, y_train, y_test = train_test_split(all_x_data, all_y_data, test_size=0.1, random_state=42)
            
            X_train = X_train / 255.0
            X_test = X_test / 255.0
            
            #training
            acc0,lr0=training()            
            
            #testing
            acc1,lr1=testing()
            
            acc_tr.append(acc0)
            lr_tr.append(lr0)
            acc_ts.append(acc1)
            lr_ts.append(lr1)
            del X_train, X_test, y_train, y_test
    
    fpr_mean = np.mean(fpr_all, axis = 0)
    fnr_mean = np.mean(fnr_all, axis = 0)
    fpr_all.append(fpr_mean)
    fnr_all.append(fnr_mean)
    print("Training Accurcay: ",(round(np.mean(acc_tr),2)))
    print("Testing Accurcay: ",(round(np.mean(acc_ts),2)))
    print("Training Loss Rate: ",(round(np.mean(lr_tr),2))
    print("Testing Loss Rate: ",(round(np.mean(lr_ts),2))
    
    # draw average DET Curve plot
    plt.plot(fpr_mean, fnr_mean, color='orange', label='DET')
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('False Negative Rate')
    plt.title('Detection Error Tradeoff (DET) Curve')
    plt.show()